import numpy as np
from mayavi.mlab import *
data = np.load('data_sample.npz')
v = data['x']

quiver3d(v[...,0],v[...,1],v[...,2])
