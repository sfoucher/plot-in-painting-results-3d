import numpy as np
from mayavi.mlab import *
data = np.load('data_sample.npz')
v = data['x']

volume_slice(np.linalg.norm(v, axis=-1), plane_orientation='x_axes')
volume_slice(np.linalg.norm(v, axis=-1), plane_orientation='y_axes')
volume_slice(np.linalg.norm(v, axis=-1), plane_orientation='z_axes')
