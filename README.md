# Visualizing 3d fluid flow

This project provides scripts to visualize 3d fluid flow scenes on Mayavi

## Getting Started

To install Mayavi follow the instructions here: https://docs.enthought.com/mayavi/mayavi/installation.html
You will also need to install numpy:
```
    pip install numpy
```
Once done you can run:
```
    mayavi2
```
in the command line. This should open the GUI.
Once there you can go to *File -> Run Python Script* and select one of the 
provided scripts.

